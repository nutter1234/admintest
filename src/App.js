import * as React from 'react'
import { Admin, Resource } from 'react-admin'
import jsonServerProvider from 'ra-data-json-server'
import { UserList, UserEdit , UserCreate } from './componant/users'
import { PostList, PostEdit, PostCreate } from './componant/sensors'
import authProvider from './componant/authProvider'
import Dashboard from './componant/dashbord'

const dataProvider = jsonServerProvider('http://localhost:3004')
const App = () => (
  <Admin
    dashboard={Dashboard}
    dataProvider={dataProvider}
    authProvider={authProvider}
  >
    <Resource
      name='users'
      list={UserList}
      edit={UserEdit}
      create={UserCreate}
    />
    <Resource
      name='sensors'
      list={PostList}
      edit={PostEdit}
      create={PostCreate}
    />
  </Admin>
)

export default App
