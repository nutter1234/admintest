import * as React from 'react'
import { useState } from "react";
import { useLogin, useNotify, Notification, defaultTheme } from "react-admin";
import { ThemeProvider } from '@material-ui/styles'
import { createMuiTheme } from '@material-ui/core/styles'
import { post } from './service/service'



export const Login = async (email, password) => {


    // const { email, password } = state
    let ret_res

    try {

        let object = {
            email: email,
            password: password,
        }
        await post(object, 'user/user_login_web', null).then((res) => {
            console.log(res)
            ret_res = res


        })
    } catch (error) {
        console.log(error)
        ret_res = { sucess: false, error_message: error }
    }

    return ret_res

}



