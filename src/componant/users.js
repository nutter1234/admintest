import * as React from 'react'
import {
  List,
  Datagrid,
  TextField,
  EmailField,
  Edit,
  EditButton,
  Create,
  SimpleForm,
  SelectInput,
  TextInput,
  ReferenceInput,
  PasswordInput,
} from 'react-admin'


export const UserList = props => (
  <List {...props}>
    <Datagrid rowClick='edit'>
      <TextField source='id' />
      <TextField source='name' />
      <EmailField source='email' />
      <TextField source='phone' />
      
      <TextField source='password' />
      <EditButton />
    </Datagrid>
  </List>
)
export const UserEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <TextInput source='id' />
      <ReferenceInput source='name' reference='users'>
        <TextInput optionText='name' />
      </ReferenceInput>
      <TextInput source='email' />
      <TextInput source='phone' />
      
      <PasswordInput source='password' />
    </SimpleForm>
  </Edit>
)
export const UserCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <TextInput disabled source='id' />
      <ReferenceInput source='name' reference='users'>
        <TextInput optionText='name' />
      </ReferenceInput>
      <TextInput source='email' />
      <TextInput source='phone' />
      
      <PasswordInput source='password' />
    </SimpleForm>
  </Create>
)
