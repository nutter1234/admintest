import * as React from 'react'
import {
  List,
  Datagrid,
  TextField,
  ReferenceField,
  EditButton,
  Edit,
  SimpleForm,
  ReferenceInput,
  SelectInput,
  TextInput,
  Create,
  AutocompleteInput
} from 'react-admin'

export const PostList = props => (
  <List {...props}>
    <Datagrid>
      <ReferenceField source='userId' reference='users'>
        <TextField source='name' />
      </ReferenceField>
      <TextField source = 'sensors' />
      <TextField source='id' />
      <TextField source='status' />
      <TextField source='chanel' />
      <EditButton />
    </Datagrid>
  </List>
)

export const PostEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <TextInput disabled source='id' />
      <TextInput source = 'sensors' />
      <ReferenceInput source='userId' reference='users'>
        <SelectInput optionText='name' />
      </ReferenceInput>
      <AutocompleteInput
        source='status'
        choices={[
          { id: 'online', name: 'online' },
          { id: 'off-line', name: 'off-line' }
        ]}
      />


      <TextInput multiline source='chanel' />
    </SimpleForm>
  </Edit>
)

export const PostCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <ReferenceInput source='userId' reference='users'>
        <SelectInput optionText='name' />
      </ReferenceInput>
      <TextInput source = 'sensors' />
      <AutocompleteInput
        source='status'
        choices={[
          { id: 'online', name: 'online' },
          { id: 'off-line', name: 'off-line' }
        ]}
      />
      <TextInput multiline source='chanel' />
    </SimpleForm>
  </Create>
)
