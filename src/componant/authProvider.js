import * as React from 'react'
import { Login } from "../Login"
import { post } from '../service/service'
export default {
    login: async ({ username, password }) => {


        try {
            let object = {
                email: username,
                password: password,
            }
            await post(object, 'user/user_login_web', null).then((res) => {
                if (res.success) {
                    localStorage.setItem('user_token', res.token)
                    return Promise.resolve()
                } else {
                    return Promise.reject()
                }

            })
        } catch (error) {
            console.log(error)
            return Promise.reject()

        }
    },

    logout: () => {
        // localStorage.removeItem('username')
        localStorage.removeItem('user_token')
        return Promise.resolve()
    },
    checkError: ({ status }) => {
        if (status === 401 || status === 403) {
            // localStorage.removeItem('username')
            localStorage.removeItem('user_token')
            return Promise.resolve()

        }
        return Promise.resolve()
    },
    checkAuth: () => {
        return localStorage.getItem('user_token') ? Promise.resolve() : Promise.reject()
    },
    getPermissions: () => Promise.resolve()

}