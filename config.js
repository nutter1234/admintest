import 'firebase/auth';

const firebaseConfig = firebase.initializeApp({
    apiKey: "AIzaSyCbP0EV_VA6TB2MDKVgoLvIRo03xMJDGMc",
    authDomain: "react-auth-fa8f6.firebaseapp.com",
    projectId: "react-auth-fa8f6",
    storageBucket: "react-auth-fa8f6.appspot.com",
    messagingSenderId: "537764508657",
    appId: "1:537764508657:web:106bf0ddd2e1fbf4380a50",
    measurementId: "G-K2P5E45N07"
});

export default 